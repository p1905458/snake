import { Game } from "./Game.js"
const req = new XMLHttpRequest();
const canvas = document.getElementById("canvas");
const menu = document.getElementById("menu");
const menuLevel = document.querySelector(".menu-level");
const gameZone = document.getElementById("game");
const score = document.getElementById("score");
const lives = document.getElementById("lives");
const gameOver = document.getElementById("gameovermodal");
const scoreBoard = document.getElementById("scoreboard");
const playerName = document.querySelector("input");
const speed = document.getElementById("speed");
const audio = new Audio("sources/sound/main.mp3");
const startSound = new Audio("sources/sound/onStart.mp3");
const eatSound = new Audio("sources/sound/onEat.mp3");
const dieSound = new Audio("sources/sound/onDie.mp3");
const gameOverSound = new Audio("sources/sound/onGameOver.mp3");
const game = new Game(canvas);
const scores = [];
let inGame = false;

let gameOverModal;


audio.loop = true;
audio.volume = 0.1;
startSound.volume = 0.2;
eatSound.volume = 0.2;
dieSound.volume = 0.2;
gameOverSound.volume = 0.2;

req.open("GET", "sources/levels.json");
req.onerror = () => console.error("Fail to load XML request");

req.onload = () => {
    let levels;
    if (req.status === 200)
        levels = JSON.parse(req.responseText);
    else
        levels = null;
    loadLevels(levels);
};

req.send();

game.onStart = () => {
    inGame = true;
    updateLives(game.lives);
    updateScore(game.score);
    menu.classList.add("invisible");
    gameZone.classList.remove("invisible");
    canvas.width  = gameZone.clientWidth;
    canvas.height = gameZone.clientHeight;
};

game.onStop = () => {
    inGame = false;
    menu.classList.remove("invisible");
    gameZone.classList.add("invisible");
};

game.onEat = (score) => {
    eatSound.play();
    updateScore(score);
    updateLives(game.lives);
};

game.onDie = lives => {
    dieSound.currentTime = 0;
    dieSound.play();
    updateLives(lives);
};

game.onGameOver = (score) => {
    dieSound.pause();
    gameOverSound.play();
    gameOver.querySelector("span").innerText = score;
    gameOverModal.open();
    updateLives(0);

    if (score > 0) {
        scores.push({name: playerName.value, score: score});
        reloadScoreBoard();
    }
};

function loadLevels(levels) {
    if (!levels) {
        menuLevel.innerHTML = "";
        menuLevel.insertAdjacentHTML("beforeend", "<button class=\"col s8 offset-s2\" disabled>Fail to load levels :/</button>")
    } else {
        for (const [name, level] of Object.entries(levels)) {
            const b = document.createElement("button");
            b.classList.add("col", "s8", "offset-s2");
            b.innerText = name;
            menuLevel.insertAdjacentElement("beforeend", b);
            b.addEventListener("click", () => loadGame(level))
        }
    }
}

function loadGame(data) {
    try {
        game.load(data);
        game.start();
        startSound.play();
    } catch (e) {
        alert("Error on level loading: "+e);
        window.location.reload();
    }
}

function updateScore(s) {
    score.innerText = `Score: ${s}`;
    speed.innerText = `Speed: ${game.snakeSpeed} ms`;
}

function updateLives(l) {
    lives.innerText = `Lives: ${l}`;
}

function reloadScoreBoard() {
    scoreBoard.querySelectorAll("div").forEach(d => d.remove());
    for (const s of scores.sort((a, b) => a.score - b.score).reverse()) {
        const e = document.createElement("div");
        e.classList.add("row-score");
        const p1 = document.createElement("p");
        p1.classList.add("player-score");
        p1.innerText = s.score;
        e.insertAdjacentElement("beforeend", p1);
        const p2 = document.createElement("p");
        p2.classList.add("pseudo");
        p2.innerText = s.name ? s.name : "Unknown";
        e.insertAdjacentElement("beforeend", p2);
        scoreBoard.insertAdjacentElement("beforeend", e);
    }
}


document.addEventListener('DOMContentLoaded', () => {
    M.AutoInit();
    gameOverModal = M.Modal.getInstance(gameOver);
});

document.addEventListener("keydown", ev => {
    if (inGame && ev.key === "Escape")
        alert("Game in pause...");
});

audio.addEventListener("canplaythrough", () => {
    audio.play();
});

scoreBoard.addEventListener("mouseover", () => {
    scoreBoard.classList.remove("close");
});

scoreBoard.addEventListener("mouseout", () => {
    scoreBoard.classList.add("close");
});

playerName.addEventListener("focusin", () => {
    playerName.placeholder = "";
});

playerName.addEventListener("focusout", () => {
    playerName.placeholder = "Choose a nickname :";
});

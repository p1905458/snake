# snake

## Features
- [x] Base of the snake game
  
- [x] Level selection

- [x] Avoid suicide by inputting the opposite direction
  
- [x] Life

- [x] Heart item to regain a life

- [x] No wall option

- [x] Score & lives display

- [x] Increase speed every 5 points

- [x] Power up to decrease the snake size

- [x] Pause game when press escape

- [x] Sprites
